namespace AvaloniaApplication1.Models;

public class File(string title, string content)
{
    public string Title
    {
        get;
        set;
    } = title;

    public string Content
    {
        get;
        set;
    } = content;
}