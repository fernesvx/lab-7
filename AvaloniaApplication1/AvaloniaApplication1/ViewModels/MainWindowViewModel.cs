﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reactive;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using ReactiveUI;
using File = AvaloniaApplication1.Models.File;

namespace AvaloniaApplication1.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private ObservableCollection<File> _files;
    public ReactiveCommand<Unit, Unit> ExportarCommand { get; set; }
    public ReactiveCommand<Unit, Unit> ImportarCommand { get; set; }

    public ObservableCollection<File> Files
    {
        get => _files;
        set => this.RaiseAndSetIfChanged(ref _files, value);
    }

    public MainWindowViewModel()
    {
        _files = new ObservableCollection<File>();
        _files.Add(new File(string.Empty, string.Empty));
        ExportarCommand = ReactiveCommand.Create(Exportar);
        ImportarCommand = ReactiveCommand.Create(Importar);
    }

    private async void Exportar()
    {
        try
        {
            var openFolderDialog = new OpenFolderDialog();
            openFolderDialog.Title = "Selecione onde exportar o arquivo";
            openFolderDialog.Directory = "../../../FilesSaved";
            var mainWindow = (Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime)
                ?.MainWindow;
            var selectedFolder = await openFolderDialog.ShowAsync(mainWindow);

            foreach (var file in Files)
            {
                var filePath = Path.Combine(selectedFolder, $"{file.Title}.txt");
                using (var writer = new StreamWriter(filePath))
                {
                    await writer.WriteAsync(file.Content);
                }
            }
        }
        catch (Exception ex)
        {
            LogError($"Erro ao exportar arquivos: {ex.Message}");
        }
    }

    public async void Importar()
    {
        try
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Selecione um arquivo para importar";
            openFileDialog.Directory = "../../../FilesSaved";
            var mainWindow = (Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime)
                ?.MainWindow;
            var selectedFile = await openFileDialog.ShowAsync(mainWindow);

            if (selectedFile != null)
            {
                using (var reader = new StreamReader(selectedFile[0]))
                {
                    string content = await reader.ReadToEndAsync();
                    string title = Path.GetFileName(selectedFile[0]).Split(".txt")[0];
                    _files.Add(new File(title, content));
                }
            }
        }
        catch (Exception ex)
        {
            LogError($"Erro ao importar arquivo: {ex.Message}");
        }
    }
    
    private void LogError(string errorMessage)
    {
        string logFileName = "error_log.txt";
        string logFilePath = Path.Combine("../../../FilesSaved", logFileName);
        string logMessage = $"{DateTime.Now}: {errorMessage}";
        
        using (StreamWriter writer = new StreamWriter(logFilePath, append: true))
        {
            writer.WriteLine(logMessage);
        }
    }
}